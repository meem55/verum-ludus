# **Verum Ludus**

### Essential Mods

* Amplified Nether
* Balm (Fabric Edition)
* Chipped
* Cloth Config API (Fabric)
* Collective (Fabric) 
* DragonLoot
* Fabric API
* Furnace Recycle (Fabric)
* Incantationem
* Iron Chests for Fabric
* Lapis Reserve
* More Axolotls
* Realistic Fire Spread
* Repurposed Structures (Fabric)
* Simple Shelves
* TrashSlot (Fabric Edition)
* Simple Voice Chat
* Harvest Scythes

### Recommended Mods

* OptiFabric
* OptiFine
* Sound Physics Remastered
* Starlight (Fabric)

### Cosmetic Assets

##### Mods

* Illuminations
* Not Enough Animations

##### Shader

* Complementary Shader

### Side Note: 
* Illuminations only works with shader (the shader must have bloom enabled for the effect to work)
* All mod names are literall and can be found under these names on curseforge (with the execption of optifine)
